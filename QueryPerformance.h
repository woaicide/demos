#ifndef QUERY_PERFORMANCE_H_
#define QUERY_PERFORMANCE_H_
#include "Winbase.h"
class PerformanceTick
{
public:
	PerformanceTick()
		: m_elapsed(0)
	{
		QueryPerformanceFrequency(&m_freq);
		start();
	}
	~PerformanceTick()
	{
		stop();
	}

	void start()
	{
		QueryPerformanceCounter(&m_beginTime);
	}
	void stop()
	{
		LARGE_INTEGER end_time;
		QueryPerformanceCounter(&end_time);
		m_elapsed += (end_time.QuadPart - m_beginTime.QuadPart) * 1000000 / m_freq.QuadPart;
	}
	void restart()
	{
		m_elapsed = 0;
		start();
	}
	//΢��
	double elapsed()
	{
		return static_cast<double>(m_elapsed);
	}
	//����
	double elapsed_ms()
	{
		return m_elapsed / 1000.0;
	}
	//��
	double elapsed_second()
	{
		return m_elapsed / 1000000.0;
	}

private:
	LARGE_INTEGER m_freq;
	LARGE_INTEGER m_beginTime;
	long long m_elapsed;
};

#endif //QUERY_PERFORMANCE_H_