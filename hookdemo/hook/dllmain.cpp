// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"
HINSTANCE g_moduleHandle = nullptr;
BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	{
		//MessageBox(NULL, L"successful", L"tittle", NULL);
		OutputDebugString(L"hook.dll attach");
		wchar_t name[MAX_PATH];
		GetModuleFileNameW(hModule, name, MAX_PATH);
		::LoadLibraryW(name);
		g_moduleHandle = hModule;
		DisableThreadLibraryCalls((HMODULE)hModule);
	}
	break;
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

HHOOK g_injectHook = nullptr;
extern "C" __declspec(dllexport) LRESULT CALLBACK msg_hook_proc_ov(int code,
	WPARAM wparam, LPARAM lparam)
{
	static bool hooking = true;
	MSG *msg = (MSG *)lparam;
	if (hooking && msg->message == (WM_USER + 432))
	{
		typedef BOOL(WINAPI * fn)(HHOOK);
		g_injectHook = (HHOOK)msg->lParam;
		//MessageBox(NULL, L"msg_hook_proc_ov", L"msg_hook_proc_ov", NULL);
		OutputDebugString(L"hook.dll recv msg_hook_proc_ov");

	}
	return CallNextHookEx(0, code, wparam, lparam);
}

