// remoteinjectdemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>
#include <Shellapi.h>
#include <TlHelp32.h>
#include <iostream>
#include <string>

bool Inject(DWORD dwId, WCHAR* szPath)
{
	//1 在目标进程中申请一个空间
	HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, dwId);
	LPVOID pRemoteAddress = VirtualAllocEx(
		hProcess,
		NULL,
		MAX_PATH,
		MEM_COMMIT,
		PAGE_READWRITE
	);
	//2 把dll的路径写入到对方的进程空间中
	DWORD dwWriteSize = 0;
	//写一段数据到指定进程所开辟的内存空间
	WriteProcessMemory(hProcess, pRemoteAddress, szPath, wcslen(szPath) * 2 + 2, &dwWriteSize);

	//3 创建一个远程线程，让目标进程调用LoadLibrary
	HANDLE hThread = CreateRemoteThread(
		hProcess,
		NULL,
		0,
		(LPTHREAD_START_ROUTINE)LoadLibrary,
		pRemoteAddress,
		NULL,
		NULL
	);
	WaitForSingleObject(hThread, -1);
	//4 释放申请的虚拟内存空间
	VirtualFreeEx(hProcess, pRemoteAddress, MAX_PATH, MEM_DECOMMIT);
	return 0;
}

#ifndef MAKEULONGLONG
#define MAKEULONGLONG(ldw, hdw) ((ULONGLONG(hdw) << 32) | ((ldw)&0xFFFFFFFF))
#endif

#ifndef MAXULONGLONG
#define MAXULONGLONG ((ULONGLONG) ~((ULONGLONG)0))
#endif
DWORD getProcMainThreadId(DWORD dwProcID)
{
	DWORD dwMainThreadID = 0;
	ULONGLONG ullMinCreateTime = MAXULONGLONG;

	HANDLE hThreadSnap = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
	if (hThreadSnap != INVALID_HANDLE_VALUE)
	{
		THREADENTRY32 th32;
		th32.dwSize = sizeof(THREADENTRY32);
		BOOL bOK = TRUE;
		for (bOK = Thread32First(hThreadSnap, &th32); bOK;
			bOK = Thread32Next(hThreadSnap, &th32))
		{
			if (dwMainThreadID == 0)
			{
				dwMainThreadID = th32.th32ThreadID;
			}
			if (th32.th32OwnerProcessID == dwProcID)
			{
				HANDLE hThread = OpenThread(THREAD_QUERY_INFORMATION,
					TRUE, th32.th32ThreadID);

				if (hThread)
				{
					FILETIME afTimes[4] = { 0 };
					if (GetThreadTimes(hThread,
						&afTimes[0], &afTimes[1], &afTimes[2], &afTimes[3]))
					{
						ULONGLONG ullTest = MAKEULONGLONG(afTimes[0].dwLowDateTime,
							afTimes[0].dwHighDateTime);
						if (ullTest && ullTest < ullMinCreateTime)
						{
							ullMinCreateTime = ullTest;
							dwMainThreadID = th32.th32ThreadID; // let it be main... :)
						}
					}
					CloseHandle(hThread);
				}
			}
		}
#ifndef UNDER_CE
		CloseHandle(hThreadSnap);
#else
		CloseToolhelp32Snapshot(hThreadSnap);
#endif
	}
	return dwMainThreadID;
}

bool safeInjectDll(DWORD pid, WCHAR* szPath)
{
	typedef HHOOK(WINAPI * fn)(int, HOOKPROC, HINSTANCE, DWORD);
	HMODULE user32 = GetModuleHandleW(L"USER32");
	fn set_windows_hook_ex;
	HMODULE lib = LoadLibraryW(szPath);
	LPVOID proc;
	HHOOK hook;

	if (!lib || !user32)
	{
		if (!lib)
		{
			std::wcout << L"LoadLibraryW failed:" << szPath;
		}
		if (!user32)
		{
			std::wcout << L"USER32 module not found:" << szPath;
		}
		return false;
	}

#ifdef _WIN64
	proc = GetProcAddress(lib, "msg_hook_proc_ov");
#else
	proc = GetProcAddress(lib, "_msg_hook_proc_ov@12");
#endif

	if (!proc)
	{
		return false;
	}

	set_windows_hook_ex = (fn)GetProcAddress(user32, "SetWindowsHookExA");
	DWORD threadId = getProcMainThreadId(pid);

	std::wcout << "hook "
		<< "pid: " << pid << ", thread:" << threadId;

	hook = set_windows_hook_ex(WH_GETMESSAGE, (HOOKPROC)proc, lib, threadId);
	if (!hook)
	{
		DWORD err = GetLastError();
		std::wcout << L"SetWindowsHookEx failed: " << err;
		return false;
	}

	for (auto i = 0; i < 2; i++)
	{
		Sleep(500);
		std::wcout << L"PostThreadMessage to hook window";

		PostThreadMessage(threadId, WM_USER + 432, 0, (LPARAM)hook);
	}
	return true;
}

wchar_t * path = L"hook.dll";
DWORD SearchProcessByName(const wchar_t* processName)
{
	DWORD pid = 0;
	ULONGLONG ullMinCreateTime = MAXULONGLONG;

	HANDLE hThreadSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hThreadSnap != INVALID_HANDLE_VALUE)
	{
		PROCESSENTRY32  entry;
		entry.dwSize = sizeof(PROCESSENTRY32);
		BOOL bOK = TRUE;
		for (bOK = Process32First(hThreadSnap, &entry); bOK;
			bOK = Process32Next(hThreadSnap, &entry))
		{
			if (_tcscmp(entry.szExeFile, processName) == 0)
			{
				pid = entry.th32ProcessID;
				break;
			}
		}
#ifndef UNDER_CE
		CloseHandle(hThreadSnap);
#else
		CloseToolhelp32Snapshot(hThreadSnap);
#endif
	}
	return pid;
}
int main()
{
	//Inject(SearchProcessByName(L"app1.exe"), path);
	safeInjectDll(SearchProcessByName(L"steam.exe"), path);
	//::Sleep(100000);
	//system("pause");
    return 0;
}

