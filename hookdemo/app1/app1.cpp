// app1.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "app1.h"
#include <stdint.h>
#include <string>

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

//=====================================================================================================
#define JMP_64_SIZE            14
#define JMP_32_SIZE            5
#define X86_NOP                0x90
#define X86_JMP_NEG_5          0xF9EB
static inline void fix_permissions(void *addr, size_t size)
{
	DWORD protect_val;
	VirtualProtect(addr, size, PAGE_EXECUTE_READWRITE, &protect_val);
}
struct func_hook {
	void                   *call_addr;

	uintptr_t              func_addr;       /* function being hooked to */
	uintptr_t              hook_addr;       /* hook function itself */
	void                   *bounce_addr;
	const char             *name;
	bool                   is_64bit_jump;
	bool                   hooked;
	bool                   started;
	bool                   attempted_bounce;
	uint8_t                unhook_data[14];
	uint8_t                rehook_data[14];
};
void hook_init(struct func_hook *hook,
	void *func_addr, void *hook_addr, const char *name)
{
	memset(hook, 0, sizeof(*hook));

	hook->func_addr = (uintptr_t)func_addr;
	hook->hook_addr = (uintptr_t)hook_addr;
	hook->name = name;

	fix_permissions((void*)(hook->func_addr),JMP_32_SIZE);

	memcpy(hook->unhook_data, func_addr, JMP_32_SIZE);
}

static inline void rehook32(struct func_hook *hook)
{
	DWORD offset = hook->hook_addr - hook->func_addr - JMP_32_SIZE;
	fix_permissions((void*)(hook->func_addr), JMP_32_SIZE);
	uint8_t *ptr = (uint8_t*)hook->func_addr;
	hook->call_addr = (void*)hook->func_addr;
	*(ptr++) = 0xE9;
	*((int32_t*)ptr) = (int32_t)offset;
	hook->hooked = true;
}

void unhook(struct func_hook *hook)
{
	uintptr_t addr;
	size_t size;

	if (!hook->hooked)
		return;

	size = JMP_32_SIZE;
	addr = hook->func_addr;

	fix_permissions((void*)addr, size);
	memcpy(hook->rehook_data, (void*)addr, size);
	memcpy((void*)hook->func_addr, hook->unhook_data, size);
	hook->hooked = false;
}
static struct func_hook g_hookdata_loadlib;
//=========================================================================================================================

typedef HMODULE(WINAPI * fn)(LPCWSTR , HANDLE , DWORD );
static HMODULE WINAPI myLoadLibrary(LPCWSTR lpLibFileName, HANDLE hFile, DWORD dwFlags)
{
	std::wstring hookfile = lpLibFileName;
	if ((hookfile.find(L"hook.dll") != std::wstring::npos) || (hookfile.find(L"ListaryHook.dll") != std::wstring::npos))
	{
		std::wstring txt = hookfile + L" want hook, reject";
		//MessageBoxW(NULL, txt.c_str(), L"", NULL);
		OutputDebugString(txt.c_str());
		return 0;
	}
	else
	{
		std::wstring txt = hookfile + L" want hook, agree";
		//MessageBoxW(NULL, txt.c_str(), L"", NULL);
		OutputDebugString(txt.c_str());
	}
	unhook(&g_hookdata_loadlib);
	fn f = (fn)g_hookdata_loadlib.call_addr;
	HMODULE ret = f(lpLibFileName, hFile, dwFlags);
	rehook32(&g_hookdata_loadlib);
	return ret;
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	HMODULE kernel32 = GetModuleHandleW(L"kernel32.dll");
	fn loadlibfunc;
#ifdef _WIN64
	loadlibfunc = (fn)GetProcAddress(kernel32, "LoadLibraryEx");
#else
	loadlibfunc = (fn)GetProcAddress(kernel32, "LoadLibraryExW");
#endif
	if (loadlibfunc)
	{
		hook_init(&g_hookdata_loadlib, (LPVOID)loadlibfunc, myLoadLibrary, "hookloadlibirary");
		rehook32(&g_hookdata_loadlib);
	}

    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_APP1, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_APP1));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_APP1));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_APP1);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
