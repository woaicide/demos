﻿#include <stdio.h>
#include <string.h>
#include "srs_librtmp.h"

int main(int argc, char** argv)
{
  if (argc < 3)
  {
    srs_human_trace("Commandline failed. \nargv[1]:rtmp://srcip/live/name \nargv[2]:rtmp://destip/live/name");
    return -1;
  }
  srs_human_trace("argv[1]:%s", argv[1]);
  srs_human_trace("argv[2]:%s", argv[2]);
#if defined(_WIN32)
  WSADATA WSAData;
  if (WSAStartup(MAKEWORD(1, 1), &WSAData))
  {
    return -1;
  }
#endif

  srs_rtmp_t rtmp_src = srs_rtmp_create(argv[1]);

  if (srs_rtmp_handshake(rtmp_src) != 0)
  {
    srs_human_trace("rtmp_src handshake failed.");
    srs_rtmp_destroy(rtmp_src);
    return -1;
  }
  srs_human_trace("rtmp_src handshake success");

  if (srs_rtmp_connect_app(rtmp_src) != 0)
  {
    srs_human_trace("rtmp_src connect vhost/app failed.");
    srs_rtmp_destroy(rtmp_src);
    return -1;
  }
  srs_human_trace("rtmp_src connect vhost/app success");

  if (srs_rtmp_play_stream(rtmp_src) != 0)
  {
    srs_human_trace("rtmp_src recv stream failed.");
    srs_rtmp_destroy(rtmp_src);
    return -1;
  }
  srs_human_trace("rtmp_src recv stream success");

  //publish
  srs_rtmp_t rtmp_dest = srs_rtmp_create(argv[2]);

  if (srs_rtmp_handshake(rtmp_dest) != 0)
  {
    srs_human_trace("rtmp_dest handshake failed.");
    srs_rtmp_destroy(rtmp_dest);
    return -1;
  }
  srs_human_trace("rtmp_dest handshake success");

  if (srs_rtmp_connect_app(rtmp_dest) != 0)
  {
    srs_human_trace("rtmp_dest connect vhost/app failed.");
    srs_rtmp_destroy(rtmp_dest);
    return -1;
  }
  srs_human_trace("rtmp_dest connect vhost/app success");

  if (srs_rtmp_publish_stream(rtmp_dest) != 0)
  {
    srs_human_trace("rtmp_dest publish stream failed.");
    srs_rtmp_destroy(rtmp_dest);
    return -1;
  }
  srs_human_trace("rtmp_dest publish stream success");

  for (;;)
  {
    int size;
    char type;
    char* data;
    u_int32_t timestamp, pts;

    int readError = srs_rtmp_read_packet(rtmp_src, &type, &timestamp, &data, &size);
    if (readError != 0)
    {
      srs_human_trace("rtmp_src recv error: %d", readError);
    }
    else
    {
      if (srs_utils_parse_timestamp(timestamp, type, data, size, &pts) == 0)
      {
        srs_human_trace("recv packet: type=%s, dts=%d, pts=%d, size=%d",
          srs_human_flv_tag_type2string(type), timestamp, pts, size);
      }
      
      char *buf = new char[size];
      memcpy(buf, data, size);
      int error = srs_rtmp_write_packet(rtmp_dest, type, timestamp, buf, size);
      if (error != 0)
      {
        srs_human_trace("===> rtmp_dest publish error: %d", error);
      }
      else
      {
        srs_human_trace("===> rtmp_dest publish success.type=%s size=%d", srs_human_flv_tag_type2string(type), size);
      }
    }

    if (data)
    { 
      delete[] data; 
      data = NULL; 
    }
  }

  srs_rtmp_destroy(rtmp_src);
  srs_rtmp_destroy(rtmp_dest);

#if defined(_WIN32)
  WSACleanup();
#endif

  return 0;
}

